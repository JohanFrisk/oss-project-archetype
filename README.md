# Archetype for HiQ OSS Projects

# Prerequisite
* Create Git repository in **oss-hiq** on bitbucket
* Git repo name and artifactId *must match*

# Install locally
```
mvn clean install archetype:update-local-catalog
```

# Run
```
mvn archetype:generate -B -DarchetypeGroupId=se.hiq.oss -DarchetypeArtifactId=oss-project-archetype -DarchetypeVersion=0.1-SNAPSHOT -DartifactId=mything -Dpackage=se.hiq.oss.mything -DprojectName='My Thing'
```
##### Optional parameters
| Variable         | Default value            |Description |
|------------------|:-------------------------|:-------------|
| parentPomVersion | 0.1                      | version of the qa-parent-pom to use |

After repo is created, enable pipeline in Bitbucket GUI.
